<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login.index');
})->middleware('guest');

Route::get('/home', function () {
    return view('dashboard.index');
})->middleware('auth')->name('dashboard');

Route::group(['middleware' => ['guest']], function () {
    Route::get('/login', 'AuthController@showLoginForm')->name('login');
    Route::post('/login', 'ConsumeApiController@attemptLogin')->name('attemptLogin');
});

Route::group(['middleware' => ['auth']], function () {
    // User Route
    Route::get('users', 'UserController@getUsers')->name('users');
    Route::get('create-user', 'UserController@createUser')->name('create-user');
    Route::post('create-user', 'UserController@createUserProcess')->name('create-user-process');
    Route::get('update-user/{id}', 'UserController@updateUser')->name('update-user');
    Route::post('update-user', 'UserController@updateUserProcess')->name('update-user-process');
    Route::get('delete-user/{id}', 'UserController@deleteUser')->name('delete-user');
    // Logout Route
    Route::get('logout', 'AuthController@logout')->name('logout');
    // Ticket Route
    Route::get('tickets', 'ConsumeApiController@attemptReport')->name('tickets');
    Route::get('create-ticket', 'TicketController@showCreate')->name('create-ticket');
    Route::post('create-ticket', 'ConsumeApiController@attemptCreate')->name('create-ticket-process');
});