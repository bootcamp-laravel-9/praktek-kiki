<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConsumeApiController extends Controller
{
    public function attemptLogin()
    {
        $request = Request::create(url('api/login'), 'POST');
        $request->headers->set('Accept', 'application/json');
        $response = app()->make('router')->dispatch($request);

        $content = json_decode($response->getContent());

        if ($content->message === 'Login Success!') {
            session(['user_data' => $content->data]);
            return redirect()->route('dashboard')->with('success', 'Login Successful!');
        } else if ($content->message === 'Login Failed, please check your credentials!') {
            return redirect()->back()->withInput()->withErrors(['error' => 'Please check your credentials!']);
        } else {
            return redirect()->back()->withInput()->withErrors(['error' => 'The account is not found!']);
        }
    }

    public function attemptReport(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $apiRequest = Request::create(url('api/get-ticket'), 'POST', $request->all());
        $apiRequest->headers->set('Accept', 'application/json');
        $response = app()->make('router')->dispatch($apiRequest);
        $content = json_decode($response->getContent());
        $totalPages = $content->data->last_page;
        if ($content->status === 'success') {
            if (empty($content->data->data)) {
                return view('ticket.report', [
                    'data' => null,
                    'total_pages' => 1,
                    'categories' => $content->categories,
                    'query' => $content->query
                ]);
            } else {
                return view('ticket.report', [
                    'data' => $content->data->data,
                    'total_pages' => $totalPages,
                    'categories' => $content->categories,
                    'query' => $content->query
                ]);
            }
        }
    }

    public function attemptCreate(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $this->validate($request, [
            'no_tiket' => 'required',
            'nama' => 'required|string|max:255|regex:/^[a-zA-Z\s]*$/',
            'email' => 'required|email|max:255',
            'no_telp' => 'required|string|max:20',
            'address' => 'required|string|max:255',
            'date_ticket' => 'required|date|after_or_equal:today',
            'total' => 'required|numeric|min:0',
            'kategori' => 'required|exists:categories,id',
        ]);

        $request = Request::create(url('api/create-ticket'), 'POST');
        $request->headers->set('Accept', 'application/json');
        $response = app()->make('router')->dispatch($request);

        $content = json_decode($response->getContent());

        if ($content->message === 'success') {
            return redirect()->route('tickets')->with('message', 'The ticket has been created')->with('alert', 'ticket-created');
        }
    }
}
