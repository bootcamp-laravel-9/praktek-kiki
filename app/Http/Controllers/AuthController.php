<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest;
use App\Repositories\AuthRepository;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected AuthRepository $AuthRepository;
    public function __construct(AuthRepository $authRepository)
    {
        $this->AuthRepository = $authRepository;
    }

    protected function redirectPath()
    {
        return route('dashboard');
    }

    public function showLoginForm()
    {
        return view('login.index');
    }

    public function login(AuthRequest $request)
    {
        try {
            $user = $this->AuthRepository->login($request);
            return $user;
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect()->intended('login');
    }
}