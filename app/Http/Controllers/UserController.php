<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function getUsers()
    {
        $users = User::paginate(5);
        $pages = ceil(count(User::all()) / 5);
        return view('user.index', [
            'users' => $users,
            'pages' => $pages
        ]);
    }

    public function createUser()
    {
        return view('user.create');
    }

    public function createUserProcess(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'max:24', 'regex:/^[a-zA-Z\s]*$/'],
            'email' => ['required', 'max:100', 'email', 'unique:users,email'],
            'password' => ['required', 'min:8', 'confirmed', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/'],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request['password']),
        ]);

        return redirect()->route('create-user')->with('message', 'The admin account has created')->with('alert', 'user-created');
        // withInput($request->except('password'));
    }

    public function updateUser($id)
    {
        $user = User::find($id);
        return view('user.update', compact('user'));
    }

    public function updateUserProcess(Request $request, User $user)
    {
        $this->validate($request, [
            'name' => ['required', 'max:24', 'regex:/^[a-zA-Z\s]*$/'],
            'email' => ['required', 'max:100', 'email'],
            'password' => ['nullable', 'min:8', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/'],
        ]);

        $user = $user->find($request->id);

        $user->name = $request->name;
        $user->email = $request->email;

        if ($request->email != $request->old_email) {
            $this->validate($request, [
                'email' => ['unique:users,email'],
            ]);
        }

        if ($request->password) {
            $user->password = Hash::make($request->password);
        }

        $user->save();

        return redirect()->intended('users')->with('message', 'The admin '.$request->old_email.' has been updated')->with('alert', 'user-updated');
    }

    public function deleteUser($id)
    {
        $detail = User::destroy($id);
        return redirect()->intended('users')->with('message', 'The admin account has been deleted');
    }
}
