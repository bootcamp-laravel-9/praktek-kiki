<?php

namespace App\Http\Controllers;

use App\Http\Requests\TicketRequest;
use App\Repositories\TicketRepository;

class TicketController extends Controller
{

    private TicketRepository $ticketRepository;

    public function __construct(TicketRepository $ticketRepository)
    {
        $this->ticketRepository = $ticketRepository;
    }

    public function createReport()
    {
        $data = $this->ticketRepository->createReport(request());
        $category = $this->ticketRepository->getCategory();
        return response()->json([
            'code' => 200,
            'status' => 'success',
            'message' => 'Success to get data ticket.',
            'data' => $data,
            'categories' => $category,
            'query' => request()->query()
        ]);
    }

    public function createTicket(TicketRequest $request)
    {
        try {
            $data = $this->ticketRepository->createTicket($request);
            return $data;
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function showCreate()
    {
        $category = $this->ticketRepository->getCategory();
        $ticketNumber = 'TCK-' . now()->unix();
        date_default_timezone_set('Asia/Jakarta');
        $minDate = date('Y-m-d');
        // dd($category);
        return view('ticket.create', [
            'categories' => $category,
            'ticketNumber' => $ticketNumber,
            'minDate' => $minDate
        ]);
    }
}