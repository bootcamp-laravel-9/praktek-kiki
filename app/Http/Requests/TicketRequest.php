<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'no_tiket' => 'required',
            'nama' => 'required|string|max:255|regex:/^[a-zA-Z\s]*$/',
            'email' => 'required|email|max:255',
            'no_telp' => 'required|string|max:20',
            'address' => 'required|string|max:255',
            'date_ticket' => 'required|date',
            'total' => 'required|numeric|min:0',
            'kategori' => 'required|exists:categories,id',
        ];
    }

    public function messages()
    {
        return [
            'nama.regex' => 'Invalid name format'
        ];
    }
}
