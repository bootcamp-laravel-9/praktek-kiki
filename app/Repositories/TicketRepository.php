<?php

namespace App\Repositories;

use App\Http\Requests\TicketRequest;
use App\Models\Category;
use App\Models\Detail;
use App\Models\Header;
use Illuminate\Support\Facades\DB;

class TicketRepository
{
    public function getCategory()
    {
        return Category::all();
    }

    public function createReport($request)
    {
        $search = strtolower($request->query('q'));
        $dateBefore = $request->query('date_before');
        $dateAfter = $request->query('date_after');
        $category = $request->query('kategori');

        if ($dateBefore && !isset($dateAfter)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Date after must be filled'
            ]);
        } elseif ($dateAfter && !isset($dateBefore)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Date before must be filled'
            ]);
        }

        $data = DB::table('headers')
            ->join('details', 'headers.id', '=', 'details.ticket_header_id')
            ->join('categories', 'details.ticket_category_id', '=', 'categories.id')
            ->select('headers.*', 'details.*', 'categories.name as category_name');

        if ($search) {
            $data->where(function ($searchquery) use ($search) {
                $searchquery
                    ->where('headers.no_tiket', 'like', '%' . $search . '%')
                    ->orWhere('headers.nama', 'like', '%' . $search . '%')
                    ->orWhere('headers.email', 'like', '%' . $search . '%')
                    ->orWhere('headers.no_telp', 'like', '%' . $search . '%')
                    ->orWhere('headers.address', 'like', '%' . $search . '%')
                    ->orWhere('categories.name', 'like', '%' . $search . '%');
            });
        }

        if ($category) {
            $data->where('categories.id', $category);
        }

        if ($dateBefore && $dateAfter) {
            $data->whereBetween('headers.date_ticket', [$dateBefore, $dateAfter]);
        }

        $result = $data->paginate(5);
        return $result;
    }

    public function createTicket(TicketRequest $request)
    {
        // dd($request);
        $validated = $request->validated();
        // dd($validated);
        $ticketHeader = Header::create([
            'no_tiket' => $validated['no_tiket'],
            'nama' => $validated['nama'],
            'email' => $validated['email'],
            'no_telp' => $validated['no_telp'],
            'address' => $validated['address'],
            'date_ticket' => $validated['date_ticket']
        ]);

        Detail::create([
            'ticket_header_id' => $ticketHeader->id,
            'ticket_category_id' => $validated['kategori'],
            'total_ticket' => $validated['total']
        ]);

        return response()->json([
            'message' => 'success',
            'data' => $ticketHeader
        ]);
    }
}
