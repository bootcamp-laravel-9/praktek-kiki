<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthRepository
{
    protected User $User;
    public function __construct(User $user)
    {
        $this->User = $user;
    }

    public function login($request)
    {
        $credentials = $request->only('email', 'password');
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Login Failed, please check your credentials!'
            ], 400);
        } 

        $user = User::where('email', $request->email)->first();

        $user['token'] = $user->createToken(config('app.name'))->plainTextToken;

        return response()->json([
            'status' => 'success',
            'message' => 'Login Success!',
            'data' => $user
        ], 200);
    }
}
