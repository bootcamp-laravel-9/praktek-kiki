@extends('layout/main')
@section('page-name', 'Update Admin')
@section('title', 'Update Admin')
@section('breadcrumbs', 'Update Admin')
@section('menu-admin', 'active')
@section('content')

<div>
    <div class="card mb-4">
        <div class="card-header pb-0 d-flex justify-content-between align-items-start mb-2">
            <div>
                <h6>Update Administrator</h6>
                <p class="text-xs">You can create, update, and delete all administrator.</p>
            </div>
        </div>
        <div class="card-body px-4 pt-0 pb-2">
            <form action="{{ route('update-user-process') }}" method="POST">
                @csrf
                <input type="hidden" name="id" value="{{ $user['id'] }}">
                <input type="hidden" name="old_email" value="{{ $user['email'] }}">
                <input type="hidden" name="old_password" value="{{ $user['password'] }}">
                <div class="form-group @error('name') has-danger @enderror">
                    <label for="name">Name</label>
                    <div class="input-group">
                        <input type="text" class="form-control @error('name') border border-danger @enderror" id="name" name="name" value="{{ $user['name'] }}" placeholder="Enter your name" required>
                    </div>
                    @error('name')
                        <small class="text-danger text-xs">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group @error('email') has-danger @enderror">
                    <label for="email">Email</label>
                    <div class="input-group">
                        <input type="email" class="form-control @error('email') border border-danger @enderror" id="email" name="email" value="{{ $user['email'] }}" placeholder="Enter your email" required>
                    </div>
                    @error('email')
                        <small class="text-danger text-xs">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group @error('password') has-danger @enderror">
                    <label for="password">Password</label>
                    <div class="input-group">
                        <input type="password" class="form-control @error('password') border border-danger @enderror" id="password" name="password" value="" placeholder="Left it blank if you not want to update the password">
                    </div>
                    @error('password')
                        <small class="text-danger text-xs">{{ $message }}</small>
                    @enderror
                </div>
                <button type="submit" class="btn bg-gradient-warning btn-sm m-0 my-4">Update</button>
                <a href="/users" class="btn bg-gradient-dark btn-sm m-0 my-4 ms-2">Back</a>
            </form>
        </div>
    </div>
</div>

@endsection
