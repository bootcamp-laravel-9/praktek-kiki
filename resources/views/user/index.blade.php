@extends('layout/main')
@section('page-name', 'Admin')
@section('title', 'Admin List')
@section('breadcrumbs', 'Admin List')
@section('menu-admin', 'active')
@section('content')

    <div class="card mb-4">
        <div class="card-header pb-0 d-flex justify-content-between align-items-start mb-2">
            <div>
                <h6>List of Administrator</h6>
                <p class="text-xs">You can create, update, and delete all administrator.</p>
            </div>
            <a href="create-user" class="btn bg-gradient-primary btn-sm m-0" data-toggle="tooltip"
                data-original-title="Edit user">
                Create
            </a>
        </div>
        <div class="card-body px-0 pt-0 pb-3">
            <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                    <thead>
                        <tr>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"
                                style="width: 35%;">Nama</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2"
                                style="width: 25%;">Email</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"
                                style="width: 20%;">Status</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"
                                style="width: 20%;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>
                                    <p class="text-xs font-weight-bold mb-0 px-3">{{ $user->name }}</p>
                                </td>
                                <td>
                                    <p class="text-xs mb-0">{{ $user->email }}</p>
                                </td>
                                <td class="align-middle text-center text-sm">
                                    @if (session('user_data')->id == $user->id)
                                        <span class="badge badge-sm bg-gradient-success">Your Account</span>
                                    @else
                                        <span class="badge badge-sm bg-gradient-secondary">Others Account</span>
                                    @endif
                                </td>
                                <td class="d-flex align-items-center justify-content-center">
                                    <a href="{{ url('update-user/' . $user->id) }}"
                                        class="btn bg-gradient-warning btn-sm m-0" data-toggle="tooltip"
                                        data-original-title="Edit user">
                                        Edit
                                    </a>
                                    <a href="javascript:void(0);"
                                        onclick="confirmDelete('{{ url('delete-user/' . $user->id) }}')"
                                        class="btn bg-gradient-danger btn-sm ms-2 m-0" data-toggle="tooltip"
                                        data-original-title="Delete user">
                                        Delete
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <nav aria-label="Page navigation mt-4">
        <ul class="pagination justify-content-end me-2">
            <li>
                <p class='mt-1 me-2'>Page</p>
            </li>
            @for ($i = 1; $i <= $pages; $i++)
                <li class="page-item ms-2">
                    <a class="page-link {{ request('page') == $i ? 'bg-gradient-secondary text-white' : '' }}"
                        href="?page={{ $i }}">
                        {{ $i }}
                    </a>
                </li>
            @endfor
        </ul>
    </nav>

    @if (session('alert') == 'user-updated')
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Updated!',
                text: '{{ session('message') }}',
                timer: 3000,
                timerProgressBar: true,
            }).then((result) => {
                if (!window.location.search.includes('page=')) {
                    window.location.search += 'page=1';
                }
            });
        </script>
    @endif

    <script>
        function confirmDelete(url) {
            Swal.fire({
                title: 'Are you sure you want to delete this user?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = url;
                }
            });
        }
    </script>

@endsection
