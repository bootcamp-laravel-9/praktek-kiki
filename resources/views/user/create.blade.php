@extends('layout/main')
@section('page-name', 'Create Admin')
@section('title', 'Create Admin')
@section('breadcrumbs', 'Create Admin')
@section('menu-add-admin', 'active')
@section('content')

<div>
    <div class="card mb-4">
        <div class="card-header pb-0 d-flex justify-content-between align-items-start mb-2">
            <div>
                <h6>Create Administrator</h6>
                <p class="text-xs">You can create, update, and delete all administrator.</p>
            </div>
        </div>
        <div class="card-body px-4 pt-0 pb-2">
            <form action="{{ route('create-user-process') }}" method="POST">
                @csrf
                <div class="form-group @error('name') has-danger @enderror">
                    <label for="name">Name</label>
                    <div class="input-group">
                        <input type="text" class="form-control @error('name') border border-danger @enderror" id="name" name="name" value="{{ old('name') }}" placeholder="Enter your name">
                    </div>
                    @error('name')
                        <small class="text-danger text-xs">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group @error('email') has-danger @enderror">
                    <label for="email">Email</label>
                    <div class="input-group">
                        <input type="email" class="form-control @error('email') border border-danger @enderror" id="email" name="email" value="{{ old('email') }}" placeholder="Enter your email">
                    </div>
                    @error('email')
                        <small class="text-danger text-xs">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group @error('password') has-danger @enderror">
                    <label for="password">Password</label>
                    <div class="input-group">
                        <input type="password" class="form-control @error('password') border border-danger @enderror" id="password" name="password" placeholder="Enter your password">
                    </div>
                    @error('password')
                        <small class="text-danger text-xs">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group @error('password_confirmation') has-danger @enderror">
                    <label for="password_confirmation">Password Confirmation</label>
                    <div class="input-group">
                        <input type="password" class="form-control @error('password_confirmation') border border-danger @enderror" id="password_confirmation" name="password_confirmation" placeholder="Re-Enter your password">
                    </div>
                    @error('password_confirmation')
                        <small class="text-danger text-xs">{{ $message }}</small>
                    @enderror
                </div>
                <button type="submit" class="btn bg-gradient-primary btn-sm m-0 my-4">Create</button>
                <a href="/users" class="btn bg-gradient-dark btn-sm m-0 my-4 ms-2">Back</a>
            </form>
        </div>
    </div>
</div>

@if (session('alert') == 'user-created')
    <script>
        Swal.fire({
            icon: 'success',
            title: 'Success!',
            text: '{{ session('message') }}',
            timer: 3000,
            timerProgressBar: true,
        });
    </script>
@endif

@endsection
