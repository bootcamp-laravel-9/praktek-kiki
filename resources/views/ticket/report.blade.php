@extends('layout/main')
@section('page-name', 'Ticket Report')
@section('title', 'Ticket Report')
@section('breadcrumbs', 'Ticket Report')
@section('menu-ticket-report', 'active')
@section('content')

    <div class="card mb-4">
        <div class="card-header pb-0 d-flex justify-content-between align-items-start mb-2">
            <div>
                <h6>List of Ticket</h6>
                <p class="text-xs">You can create and see all report of ticket.</p>
            </div>
            <a href="/tickets" class="btn bg-gradient-dark btn-s m-0 me-2" data-toggle="tooltip"
                data-original-title="Edit user">
                <i class="fas fa-sync-alt me-2"></i> Refresh
            </a>
        </div>
        <form method="GET" action="/tickets">
            <div class="row mb-3 ms-2 me-4">
                <div class="col-md-3">
                    <div class="position-relative">
                        <input type="text" class="form-control pe-5" id="search" name="q" placeholder="Search"
                            value="{{ request('q') }}">
                        <span class="clear-search-icon position-absolute top-0 end-2 end-0 pt-2 mx-2" id="clear-search">
                            <i class="fa fa-times icon-x"></i>
                        </span>
                    </div>

                    <style>
                        .clear-search-icon {
                            cursor: pointer;
                        }
                    </style>

                    <script>
                        document.getElementById('clear-search').addEventListener('click', function() {
                            document.getElementById('search').value = '';
                        });
                    </script>
                </div>
                <div class="col-md-2">
                    <input type="date" class="form-control" id="date_before" name="date_before" placeholder="Date Before"
                        value="{{ request('date_before') }}">
                </div>
                <div class="col-md-2">
                    <input type="date" class="form-control" id="date_after" name="date_after" placeholder="Date After"
                        value="{{ request('date_after') }}">
                </div>
                <div class="col-md-3">
                    <select class="form-select" id="kategori" name="kategori">
                        <option selected disabled>Select Category</option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}"
                                {{ request('kategori') == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary bg-gradient-primary w-100">FIND</button>
                </div>
            </div>
        </form>
        <div class="card-body px-0 pt-0 pb-3">
            <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                    <thead>
                        <tr>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">No</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nomor Tiket</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nama</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Email</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">No Telpon</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Kategori</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Tgl Tiket</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Total</th>
                            {{-- <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Action</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @if ($data == null)
                            <tr>
                                <td colspan="9" class="text-center">Data tidak ditemukan</td>
                            </tr>
                        @else
                            @foreach ($data as $value)
                                <tr>
                                    <td class="text-xs mb-0 px-4 py-3">{{ $loop->iteration }}.</td>
                                    <td class="text-xs mb-0 px-4">{{ $value->no_tiket }}</td>
                                    <td class="text-xs mb-0 px-4">{{ $value->nama }}</td>
                                    <td class="text-xs mb-0 px-4">{{ $value->email }}</td>
                                    <td class="text-xs mb-0 px-4">{{ $value->no_telp }}</td>
                                    <td class="text-xs mb-0 px-4">{{ $value->category_name }}</td>
                                    <td class="text-xs mb-0 px-4">{{ $value->date_ticket }}</td>
                                    <td class="text-xs mb-0 px-4">{{ $value->total_ticket }}</td>
                                    {{-- <td class="d-flex align-items-center justify-content-center">
                                        <a href="javascript:void(0);"
                                            onclick="confirmDelete('{{ url('delete-ticket/' . $value->id) }}')"
                                            class="btn bg-gradient-danger btn-s ms-2 me-4 m-0" data-toggle="tooltip"
                                            data-original-title="Delete Ticket">
                                            Delete
                                        </a>
                                    </td> --}}
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <nav aria-label="Page navigation mt-4">
        <ul class="pagination justify-content-end me-2">
            <li>
                <p class='mt-1 me-2'>Page</p>
            </li>
            @for ($i = 1; $i <= $total_pages; $i++)
                <li class="page-item ms-2">
                    <a class="page-link {{ request('page') == $i ? 'bg-gradient-secondary text-white' : '' }}"
                        href="?page={{ $i }}">
                        {{ $i }}
                    </a>
                </li>
            @endfor
        </ul>
    </nav>

    {{-- <script>
        document.addEventListener('DOMContentLoaded', (event) => {
            if (!window.location.search.includes('page=')) {
                window.location.search += 'page=1';
            }
        });
    </script> --}}

    @if (session('alert') == 'ticket-created')
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Success!',
                text: '{{ session('message') }}',
                timer: 3000,
                timerProgressBar: true,
            });
        </script>
    @endif

@endsection
