@extends('layout/main')
@section('page-name', 'Create Ticket')
@section('title', 'Create Ticket')
@section('breadcrumbs', 'Create Ticket')
@section('menu-create-ticket', 'active')
@section('content')

    <div>
        <div class="card mb-4">
            <div class="card-header pb-0 d-flex justify-content-between align-items-start mb-2">
                <div>
                    <h6>Create New Ticket</h6>
                    <p class="text-xs">You can create and see all report of ticket.</p>
                </div>
            </div>
            <div class="card-body px-4 pt-0 pb-2">
                <form action="{{ route('create-ticket-process') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group @error('no_tiket') has-danger @enderror">
                                <label for="no_tiket">Ticket Number</label>
                                <div class="input-group">
                                    <input type="text"
                                        class="form-control @error('no_tiket') border border-danger @enderror"
                                        id="no_tiket" name="no_tiket" value="{{ $ticketNumber }}"
                                        placeholder="Ticket Number" disabled>
                                    <input type="hidden" name="no_tiket" value="{{ $ticketNumber }}">
                                </div>
                                @error('no_tiket')
                                    <small class="text-danger text-xs">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group @error('nama') has-danger @enderror">
                                <label for="nama">Name</label>
                                <div class="input-group">
                                    <input type="text" class="form-control @error('nama') border border-danger @enderror"
                                        id="nama" name="nama" value="{{ old('nama') }}"
                                        placeholder="Enter your name">
                                </div>
                                @error('nama')
                                    <small class="text-danger text-xs">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group @error('email') has-danger @enderror">
                                <label for="email">Email</label>
                                <div class="input-group">
                                    <input type="email"
                                        class="form-control @error('email') border border-danger @enderror" id="email"
                                        name="email" value="{{ old('email') }}" placeholder="Enter your email">
                                </div>
                                @error('email')
                                    <small class="text-danger text-xs">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group @error('no_telp') has-danger @enderror">
                                <label for="no_telp">Phone Number</label>
                                <div class="input-group">
                                    <input type="text"
                                        class="form-control @error('no_telp') border border-danger @enderror" id="no_telp"
                                        name="no_telp" value="{{ old('no_telp') }}" placeholder="Enter your phone number"
                                    >
                                </div>
                                @error('no_telp')
                                    <small class="text-danger text-xs">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group @error('address') has-danger @enderror">
                        <label for="address">Address</label>
                        <div class="input-group">
                            <textarea class="form-control @error('address') border border-danger @enderror" id="address" name="address"
                                placeholder="Enter your address">{{ old('address') }}</textarea>
                        </div>
                        @error('address')
                            <small class="text-danger text-xs">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group @error('date_ticket') has-danger @enderror">
                                <label for="date_ticket">Ticket Date</label>
                                <div class="input-group">
                                    <input type="date"
                                        class="form-control @error('date_ticket') border border-danger @enderror"
                                        id="date_ticket" name="date_ticket" value="{{ old('date_ticket') }}"
                                        min="{{ $minDate }}">
                                </div>
                                @error('date_ticket')
                                    <small class="text-danger text-xs">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group @error('total') has-danger @enderror">
                                <label for="total">Total Ticket</label>
                                <div class="input-group">
                                    <input type="number" min="1" max="25"
                                        class="form-control @error('total') border border-danger @enderror" id="total"
                                        name="total" value="{{ old('total') }}" placeholder="Enter total ticket"
                                    >
                                </div>
                                @error('total')
                                    <small class="text-danger text-xs">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group @error('kategori') has-danger @enderror">
                        <label for="kategori">Category</label>
                        <div class="input-group">
                            <select class="form-select @error('kategori') border border-danger @enderror" id="kategori"
                                name="kategori">
                                <option value="">Select category</option>
                                @foreach ($categories as $value)
                                    <option value="{{ $value->id }}" @if (old('kategori') == $value->id) selected @endif>
                                        {{ $value->name }} | {{ $value->detail }}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('kategori')
                            <small class="text-danger text-xs">{{ $message }}</small>
                        @enderror
                    </div>
                    <button type="submit" class="btn bg-gradient-primary btn-sm m-0 my-4">Create</button>
                    <a href="/users" class="btn bg-gradient-dark btn-sm m-0 my-4 ms-2">Back</a>
                </form>
            </div>
        </div>
    </div>

@endsection
