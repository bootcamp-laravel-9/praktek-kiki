<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Category;
use App\Models\Detail;
use App\Models\Header;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $category = [
            ['name' => 'Jogja Tour 1', 'detail' => 'Tour keliling Kota Yogyakarta meliputi malioboro, keraton, taman sari, dan alun-alun'],
            ['name' => 'Jogja Tour 2', 'detail' => 'Tour keliling Gunung Kidul meliputi pantai indrayanti, pantai baron, dan pantai kukup'],
            ['name' => 'Jogja Tour 3', 'detail' => 'Tour keliling Bantul meliputi pantai parangtritis, gumuk pasir, dan wisata alam lainnya'],
            ['name' => 'Jogja Tour 4', 'detail' => 'Tour keliling Sleman meliputi candi prambanan dan wisata sejarah lainnya'],
            ['name' => 'Jogja Tour 5', 'detail' => 'Tour keliling Kulon Progo meliputi wisata alam, air terjun, dan wisata lainnya'],
            ['name' => 'Jogja Tour 6', 'detail' => 'Tour keliling Gunung Merapi meliputi kaliurang, wisata alam, wisata sejarah, dan wisata lainnya']
        ];

        $user = [
            ['name' => 'Admin', 'email' => 'admin@gmail.com', 'password' => Hash::make('AdminTest123!')]
        ];

        $header = [
            ['no_tiket' => 'TCK-1712173281', 'nama' => 'Mikael Rizki', 'email' => 'mikael.rizki.pe@gmail.com', 'no_telp' => '6281234567890', 'address' => 'Jl. Raya Bogor', 'date_ticket' => '2024-04-03']
        ];

        $detail = [
            ['ticket_header_id' => 1, 'ticket_category_id' => 1, 'total_ticket' => 2]
        ];

        DB::beginTransaction();

        foreach ($category as $category) {
            Category::firstOrCreate($category);
        }

        foreach ($user as $user) {
            User::firstOrCreate($user);
        }

        foreach ($header as $header) {
            Header::firstOrCreate($header);
        }

        foreach ($detail as $detail) {
            Detail::firstOrCreate($detail);
        }

        DB::commit();
    }
}
